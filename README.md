# sleep tracker

OpenGear Hackaton project September 2019.

The sleep tracker detects when a user sleeps in front of the camera.
When a user falls asleep, this program sends a message to a Slack channel that includes a picture of the sleeper and a message for all of their co-workers to see.

## install and run

```
python3 -m pip install --user --upgrade -r requirements.txt
python3 detect_sleep.py
```

## authors

- @devon.pringle
- @jeevan.kishore
- @paulo.fagundes
- @tom.caldwell

## acknowledgements

this project was based of [eye-blink-detection-demo]


[eye-blink-detection-demo]: https://github.com/mans-men/eye-blink-detection-demo
