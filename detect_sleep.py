#!/usr/bin/env python3

# import the necessary packages
from scipy.spatial import distance as dist
from imutils.video import FileVideoStream
from imutils.video import VideoStream
from imutils import face_utils
import argparse
import imutils
import time
import dlib
import cv2
import sys
from slacker import Slacker
import io
import os


def eye_aspect_ratio(eye):
    # compute the euclidean distances between the two sets of
    # vertical eye landmarks (x, y)-coordinates
    A = dist.euclidean(eye[1], eye[5])
    B = dist.euclidean(eye[2], eye[4])

    # compute the euclidean distance between the horizontal
    # eye landmark (x, y)-coordinates
    C = dist.euclidean(eye[0], eye[3])

    # compute the eye aspect ratio
    ear = (A + B) / (2.0 * C)

    # return the eye aspect ratio
    return ear


def main() :
    # initialize dlib's face detector (HOG-based) and then create
    # the facial landmark predictor
    print("[INFO] loading facial landmark predictor...")
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    # grab the indexes of the facial landmarks for the left and
    # right eye, respectively
    (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
    (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

    # start the video stream thread
    print("[INFO] starting video stream thread...")
    print("[INFO] print q to quit...")
    vs = VideoStream(src=0).start()
    fileStream = False

    time.sleep(1.0)

    debounce = 0
    asleep = False
    counter = 0

    # loop over frames from the video stream
    while True:
        # if this is a file video stream, then we need to check if
        # there any more frames left in the buffer to process
        if fileStream and not vs.more():
            break

        # grab the frame from the threaded video file stream, resize
        # it, and convert it to grayscale
        # channels)
        frame = vs.read()
        frame = imutils.resize(frame, width=450)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # detect faces inasleep = False the grayscale frame
        rects = detector(gray, 0)


        # loop over the face detections
        for rect in rects:
            # determine the facial landmarks for the face region, then
            # convert the facial landmark (x, y)-coordinates to a NumPy
            # array
            shape = predictor(gray, rect)
            shape = face_utils.shape_to_np(shape)

            # extract the left and right eye coordinates, then use the
            # coordinates to compute the eye aspect ratio for both eyes
            leftEye = shape[lStart:lEnd]
            rightEye = shape[rStart:rEnd]
            leftEAR = eye_aspect_ratio(leftEye)
            rightEAR = eye_aspect_ratio(rightEye)

            # average the eye aspect ratio together for both eyes
            ear = (leftEAR + rightEAR) / 2.0

            # check to see if the eye aspect ratio is below the blink
            # threshold, and if so, increment the blink frame counter

            eyes_closed = ear < 0.2

            wakeup_thresh = 100
            sleep_thresh = 200
            debounce_thresh = 20

            # if we are looking at a debounce
            if (eyes_closed and not asleep) or (not eyes_closed and asleep):
                debounce += 1
                if debounce > debounce_thresh:
                    pass
                else:
                    print("inc deb: {}".format(debounce))
                    continue


            if eyes_closed and not asleep:
                if counter > sleep_thresh:
                    print("fell asleep")
                    # save frame
                    cv2.imwrite('/tmp/sleep.jpg', frame)
                    # Send slack message once
                    username = os.environ['USER']
                    send_slack_photo('/tmp/sleep.jpg')
                    # send_slack_message("Video stream of {} asleep.".format(username))
                    asleep = True
                    debounce = 0
                    counter = 0
                else:
                    counter += 1
            elif eyes_closed and asleep:
                # send message
                counter = 0
                debounce = 0
                pass
            elif not eyes_closed and asleep:
                if counter > wakeup_thresh:
                    asleep = False
                    debounce = 0
                    print("woke up")
                    counter = 0
                else:
                    counter += 1
            else:   # We are awake
                pass


    # do a bit of cleanup
    cv2.destroyAllWindows()
    vs.stop()

def send_slack_message(msg):
    slackClient = Slacker("xoxb-77836304546-749575704161-z7j0FKNpztJI637MSjxdTuTS")
    slackClient.chat.post_message("#sleep", msg)

def send_slack_photo(filename):
    slackClient = Slacker("xoxb-77836304546-749575704161-z7j0FKNpztJI637MSjxdTuTS")
    slackClient.files.upload(filename, channels=['#sleep'])


if __name__ == '__main__' :
    main()
